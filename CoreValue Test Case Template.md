# CoreValue Test Case Template

This document is intended to provide a test case template that would be most suitable for automation.

The CoreValue automation team has elaborated the approach that should help implement qualified and effective test automation in reasonably short time.

## Test Case Template

We would like that every test case posesses the following attributes:
`
Id, Description, Preconditions, Steps, Data, Expected Results, Post Actions, Automation Status, Notes
`

We would like that test cases are kept in table structures as follows:

| Id | Description | Preconditions | Steps | Data | Expected Results | Post Actions | Automation Status | Notes|
|:-- |:-----------:|:-----------:|:-----:|:----:|:----------------:|:------------:|:-----------------:|:-----:|
||||||||||


####Id
**Id** attribute should be a unique, short denotation of a test case,  e.g. LP001, or TC.IV.2.a.

Ideally, the rules to generate <b>Id</b>s should be simple enough to be understood by all stakeholders.

####Description

**Description** attribute should be a concise description of the tested functionality.

For instance, it can consists of a user story, or be the name of a feature being tested.

#### Preconditions

**Preconditions** attribute should consists of the requirements needed to execute the test case.

For example, if a test case verifies that an admin user can edit a 'no-previleges' user's profile details, the preconditions may be the following:

1. An admin user exists in the system.
2. A 'no-previleges' user exists in the system.

#### Steps

**Steps** attribute

#### Data

**Expected Results** attribute

#### Post Actions

**Post Actions** attribute

#### Automation Status

**Automation Status** attribute

#### Notes

**Notes** attribute